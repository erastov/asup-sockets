var config = require('../config');
var Promise = require('bluebird');
var redis = Promise.promisifyAll(require("redis"));
var redisClient = redis.createClient(config.redis.port, config.redis.host);


redisClient.select(config.redis.db);

module.exports = redisClient;