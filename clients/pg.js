var config = require('../config');
var Promise = require('bluebird');
var pg = Promise.promisifyAll(require('pg'));
var pgClient = new pg.Client(config.pg);

pgClient.connectAsync().then(function(client) {
    client.query('LISTEN notification');
});

module.exports = pgClient;