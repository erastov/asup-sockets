var env = process.env.NODE_ENV || 'development';

var redis = {
    'development': {
        host: '192.168.128.233',
        port: 6379,
        db: 1
    },
    'production': {
        host: '192.168.128.233',
        port: 6379,
        db: 1
    }
};

var pg = {
    'development': 'postgres://postgres:mysqld@progtest:5432/opentech',
    'production': 'postgres://postgres:cdtnjx@database.opentech.local:5432/opentech'
};

module.exports = {
    redis: redis[env],
    pg: pg[env]
};