var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var SocketModel = require('./models/socket-model');
var pgClient = require('./clients/pg');

io.sockets.setMaxListeners(0);

pgClient.on('notification', function(message) {
    SocketModel.sendMessageFromPg(io, message);
});

io.on('connection', function (socket) {
    socket.on('join', function(data) {
        SocketModel.joinToRoom(socket, data);
    });
});
// example NOTIFY notification, '{"room":"265d86aa26ecf5e2d6574d1a967b0b1e","event":"orderMessage","data":{"a":"b","c":"d","e":"f"}}';
server.listen(8890);