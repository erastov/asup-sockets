var helpers = require('../helpers');
var redisClient = require('../clients/redis');

var SocketModel = {
    sendMessageFromPg: function(io, message) {
        var msg = JSON.parse(message.payload);
        var sockets = helpers.findSockets(io, msg.room);
        console.log(message);
        sockets.forEach(function(socket) {
            if (socket.hasOwnProperty('sessionId')) {
                redisClient.existsAsync(socket.sessionId).then(function(isExist) {
                    if (isExist) {
                        if (socket.hash == msg.room) {
                            socket.emit('notification', message.payload);
                        }
                    } else {
                        socket.disconnect();
                    }
                });
            }
        });
    },

    joinToRoom: function(socket, data) {
        console.log(data);

        socket.join(data.room);
        socket.hash = data.room;
        socket.sessionId = data.sessionId;
    }
};

module.exports = SocketModel;