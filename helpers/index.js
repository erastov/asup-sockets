module.exports.findSockets = function findSockets(io, roomId) {
    var ns = io.of('/'),
        res = [];
    if (ns) {
        for (var id in ns.connected) {
            if (ns.connected.hasOwnProperty(id)) {
                var rooms = ns.connected[id].rooms ? ns.connected[id].rooms : {},
                    isExist = rooms.hasOwnProperty(roomId);

                if (isExist) {
                    res.push(ns.connected[id]);
                }
            }
        }
    }
    return res;
};